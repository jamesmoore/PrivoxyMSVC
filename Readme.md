Privoxy for MSVC
================
An adaptation of the [Privoxy](https://www.privoxy.org/) web filtering proxy server for the MS Visual Studio development tools. The purpose is to enable 64 bit compilation for the windows platform so that it can be hosted on Nano Server.

Only errlog.c was changed from the original sources. Everything else (solution, project and config.h) is an addition.

Todo
----
* Include zlib feature




